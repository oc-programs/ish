--[[
basic message
...,
[6]= command,
[7] = payload,
[8] = dest_address,
...,
signature


multipart message
...,
[6]= "multipart",
[7] = part_payload or "return {command=\"run\", parts=10, salt="12345678"}" if part==0,
[8] = dest_address,
[9] = part,
[10] = parts,
[11] = signature
]]

self_addr = component.list("modem")()
wifi = component.proxy(self_addr)
pc = computer
pull = pc.pullSignal
passwd = "there is only one true Yuri"
alias = ""

multipart_limit = 16
multipart_msg = {}

actions = {}

function hasher(str)
  mask = 31
  csum = 0
  for i = 1, #str do
    csum = csum ~ ((str:byte(i,i)) << (i&mask))
  end
  return csum
end

function valid(M, salt)
  if M[1] == "modem_message" and M[8] == self_addr then
    salt = salt or passwd
    return M[#M] == hasher(string.sub(M[7],1,16)..salt..self_addr)
  end
end

function actions.multipart(M)
  part = M[9]
  if part == 0 then
    code, fail = load(M[7])
    if code then
      tbl = pcall(code)
      if tbl  and tbl.parts <= multipart_limit then
        multipart_msg = tbl
        multipart_msg.signature = passwd..tbl.salt
--[[        multipart_msg = {
          command = tbl.command,
          parts = tbl.part_count,
          salt  = passwd..tbl.salt
        }
--]]
      end
    end
  elseif part <= multipart_limit then
  end
end
  

function process_message(M, possible_acts)
  possible_acts = possible_acts or actions
  act = possible_acts[M[6]]
  if valid(M) and act then
      act(M)
  end
end

function actions.run(M)
  code, fail = load(M[7])
  if code then
    return {pcall(code)}
  else
    return {fail}
  end
end

function actions.set_passwd(M)
  passwd = M[7]
  return {self_addr}
end

function actions.set_alias(M)
  alias = M[7]
  return {alias}
end


wifi.setStrength(400)